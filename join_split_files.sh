#!/bin/bash

cat vendor/lib64/camera/com.qti.tuned.sm7125_sunny_imx586_19567.bin.* 2>/dev/null >> vendor/lib64/camera/com.qti.tuned.sm7125_sunny_imx586_19567.bin
rm -f vendor/lib64/camera/com.qti.tuned.sm7125_sunny_imx586_19567.bin.* 2>/dev/null
cat vendor/lib64/camera/com.qti.tuned.sm7125_sunny_imx586_19567_india.bin.* 2>/dev/null >> vendor/lib64/camera/com.qti.tuned.sm7125_sunny_imx586_19567_india.bin
rm -f vendor/lib64/camera/com.qti.tuned.sm7125_sunny_imx586_19567_india.bin.* 2>/dev/null
cat vendor/lib/camera/com.qti.tuned.sm7125_sunny_imx586_19567.bin.* 2>/dev/null >> vendor/lib/camera/com.qti.tuned.sm7125_sunny_imx586_19567.bin
rm -f vendor/lib/camera/com.qti.tuned.sm7125_sunny_imx586_19567.bin.* 2>/dev/null
cat vendor/lib/camera/com.qti.tuned.sm7125_sunny_imx586_19567_india.bin.* 2>/dev/null >> vendor/lib/camera/com.qti.tuned.sm7125_sunny_imx586_19567_india.bin
rm -f vendor/lib/camera/com.qti.tuned.sm7125_sunny_imx586_19567_india.bin.* 2>/dev/null
cat system/system/preload/OppoRelax/OppoRelax.apk.* 2>/dev/null >> system/system/preload/OppoRelax/OppoRelax.apk
rm -f system/system/preload/OppoRelax/OppoRelax.apk.* 2>/dev/null
cat system/system/apex/com.android.runtime.release.apex.* 2>/dev/null >> system/system/apex/com.android.runtime.release.apex
rm -f system/system/apex/com.android.runtime.release.apex.* 2>/dev/null
cat system/system/app/OppoCamera/OppoCamera.apk.* 2>/dev/null >> system/system/app/OppoCamera/OppoCamera.apk
rm -f system/system/app/OppoCamera/OppoCamera.apk.* 2>/dev/null
cat system/system/app/OppoEngineerCamera/OppoEngineerCamera.apk.* 2>/dev/null >> system/system/app/OppoEngineerCamera/OppoEngineerCamera.apk
rm -f system/system/app/OppoEngineerCamera/OppoEngineerCamera.apk.* 2>/dev/null
cat system/system/priv-app/SettingsEU/SettingsEU.apk.* 2>/dev/null >> system/system/priv-app/SettingsEU/SettingsEU.apk
rm -f system/system/priv-app/SettingsEU/SettingsEU.apk.* 2>/dev/null
cat system/system/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system/system/priv-app/SystemUI/SystemUI.apk
rm -f system/system/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system/system/priv-app/OppoGallery2EU/OppoGallery2EU.apk.* 2>/dev/null >> system/system/priv-app/OppoGallery2EU/OppoGallery2EU.apk
rm -f system/system/priv-app/OppoGallery2EU/OppoGallery2EU.apk.* 2>/dev/null
cat oppo_product/vendor_overlay/29/lib64/libstblur_capture_api.so.* 2>/dev/null >> oppo_product/vendor_overlay/29/lib64/libstblur_capture_api.so
rm -f oppo_product/vendor_overlay/29/lib64/libstblur_capture_api.so.* 2>/dev/null
cat oppo_product/app/Photos/Photos.apk.* 2>/dev/null >> oppo_product/app/Photos/Photos.apk
rm -f oppo_product/app/Photos/Photos.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> product/priv-app/Velvet/Velvet.apk
rm -f product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
